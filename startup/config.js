// Importing configuration details
const config = require('config')


// Checking whether jwt private key is defined or not
module.exports = function () {
    if (!config.get('jwtPrivateKey')) {
        throw new Error(`FATAL ERROR: jwt private key is not defined.`)
    }
}