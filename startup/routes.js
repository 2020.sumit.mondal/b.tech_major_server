// Importing the custom error middleware
const error = require('../middleware/error')

// Importing the body parser module
const bodyParser = require('body-parser')

// Importing the devices module
const devices = require('../routes/devices')

// Importing the users module
const users = require('../routes/users')

// Importing the auth module
const auth = require('../routes/authentication')

// Importing the cards module
const cards = require('../routes/cards')


// Exporting the function for setting up routes
module.exports = function (app) {
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({
        extended: true
    }))
    app.use('/api/devices/device', devices)
    app.use('/api/users/user', users)
    app.use('/api/auth', auth)
    app.use('/api/cards/card', cards)
    app.use(error)
}