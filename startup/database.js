// Importing the custom winston error logger
const logger = require('../logger/logger')

// Importing firebase-admin module
const admin = require('firebase-admin')
const serviceAccount = require('./database/serviceAccountKey.json')


// Function for initializing firebase
const initializeFirebase = function () {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://major-917e8.firebaseio.com"
    })
}

// Defining variable for the cloud-firestore
let firestoreDB

// Defining variable for the real time database
let realTimeDB

// Function for initializing firebase cloud-firestore
const initializeDatabase = function () {
    firestoreDB = admin.firestore()
    realTimeDB = admin.database()
}

// Initializing firebase
initializeFirebase()
logger.log('info', `Firebase initialization successful..`)

// Initializing cloud-firestore
initializeDatabase()
logger.log('info', `Cloud-Firestore initialization successful...`)
logger.log('info', `Firebase realtime database initialization successful...`)


// Exporting database details
exports.firestoreDB = firestoreDB
exports.realTimeDB = realTimeDB