// Importing the helmet module
const helmet = require('helmet')

// Importing the compression module
const compression = require('compression')


// Exporting the production module
module.exports = function (app) {
    app.use(helmet())
    app.use(compression())
}