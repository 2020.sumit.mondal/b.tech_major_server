// Importing express-async-errors module
require('express-async-errors')

// Importing the custom winston error logger
const logger = require('../logger/logger')


// Exporting the uncaught error logger module
module.exports = function () {
    // Handler for uncaught exceptions
    process.on('uncaughtException', (err) => {
        logger.log('error', err.message)
        process.exit(1)
    })

    // Handler for unhandled promise rejections
    process.on('unhandledRejection', (err) => {
        logger.log('error', err.message)
        process.exit(1)
    })
}