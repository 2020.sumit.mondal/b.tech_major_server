// Importing express module
const express = require('express')
const router = express.Router()

// Importing database details
const database = require('../startup/database')
const db = database.firestoreDB

// Importing lodash module
const _ = require('lodash')

// Importing bcrypt module
const bcrypt = require('bcrypt')

// Importing jwt generator middleware
const jwtGenerator = require('../middleware/jwtGenerator')


// Referencing Users collections
const collectionName = 'Users'

/* Route for authenticating a particular user */
router.post('/', async (req, res) => {
    let userPresent
    let userDetails
    let validPassword

    await db.collection(collectionName).doc(`${req.body.email}`).get()
        .then(user => {
            if (!user.exists) {
                userPresent = false
            } else {
                userPresent = true
                userDetails = user._fieldsProto
            }
        })

    if (req.body.email == null || req.body.email == "" || req.body.password == null || req.body.password == "") {
        return res.status(400).send(`Please fill up all the fields`)
    } else if (userPresent) {
        const userPassword = userDetails.password.stringValue
        validPassword = await bcrypt.compare(req.body.password, userPassword)

        if (validPassword) {
            const token = jwtGenerator.generateJwt(userDetails.email.stringValue)
            return res.status(200).send({
                name: `${userDetails.name.stringValue}`,
                email: `${userDetails.email.stringValue}`,
                token: `${token}`
            })
        } else {
            return res.status(400).send(`Invalid password`)
        }
    } else {
        return res.status(400).send(`User with the given email is not present`)
    }
})


// Exporting the router module
module.exports = router