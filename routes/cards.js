// Importing express module
const express = require('express')
const router = express.Router()

// Importing database details
const database = require('../startup/database')
const db = database.firestoreDB
const realtimeDB = database.realTimeDB

// Importing lodash module
const _ = require('lodash')

// Importing jwt generator middleware
const jwtGenerator = require('../middleware/jwtGenerator')

// Importing the authorization middleware
const authorization = require('../middleware/authorization')

// Importing firebase-admin
const admin = require('firebase-admin')


// Referencing Cards collections
const cardsCollectionName = 'Cards'

// Referencing the devices collection
const ref = realtimeDB.ref("/Devices")

/* Route for creating a new card under a particular user database */
router.post('/create', authorization, async (req, res) => {
    let deviceRef = ref.child(`device${req.body.deviceId}`)
    let cardDetails = _.pick(req.body, ['name', 'deviceId'])

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            if (req.body.name == null || req.body.name == "" || req.body.deviceId == null || req.body.deviceId == "") {
                return res.status(400).send(`Please fill up all the fields`)
            } else if ((snapshot.val()).isTaken) {
                return res.status(400).send(`Device with the given id is already associated with an user`)
            } else {
                db.collection(cardsCollectionName).doc(`${req.user.email}`).update({
                    cards: admin.firestore.FieldValue.arrayUnion(cardDetails)
                }).then(() => {
                    deviceRef.update({
                        "isTaken": true
                    }).then(() => {
                        return res.send(cardDetails)
                    })
                })
            }
        } else {
            return res.status(400).send(`Device with the given id is not present`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for deleting a particular card under a particular user */
router.delete('/:deviceId/delete', authorization, async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)
    let response
    let cardDetails
    let cardsArrayLength
    let cardFound
    let updatedCards = []

    response = await db.collection(cardsCollectionName).doc(`${req.user.email}`).get()
    cardDetails = response._fieldsProto.cards.arrayValue.values
    cardsArrayLength = (cardDetails).length

    for (i = 0; i < cardsArrayLength; i++) {
        const cardsArray = cardDetails[i]

        if (cardsArray.mapValue.fields.deviceId.stringValue != req.params.deviceId) {
            const card = {
                name: cardsArray.mapValue.fields.name.stringValue,
                deviceId: cardsArray.mapValue.fields.deviceId.stringValue
            }

            updatedCards.push(card)
        } else {
            cardFound = true
        }
    }

    if (cardFound) {
        db.collection(cardsCollectionName).doc(`${req.user.email}`).update({
            cards: updatedCards
        }).then(() => {
            deviceRef.update({
                configurations: {
                    gasVolume: "29",
                    firstNotifiableVolume: "15",
                    notificationFrequencyVolume: "5"
                },
                gasUsageWastageVolume: "0",
                objectIsPresent: false,
                valveIsOpen: false,
                isTaken: false
            }).then(() => {
                return res.send(updatedCards)
            })
        })
    } else {
        return res.status(400).send('Card with the given deviceId is not present')
    }
})

/* Route for getting all the cards associated with a particular user */
router.get('/user', authorization, async (req, res) => {
    let response
    let cardDetails
    let cardsArrayLength
    let updatedCards = []

    await db.collection(cardsCollectionName).doc(`${req.user.email}`).get()
        .then(cards => {
            response = cards
        })

    cardDetails = response._fieldsProto.cards.arrayValue.values
    cardsArrayLength = (cardDetails).length

    for (i = 0; i < cardsArrayLength; i++) {
        const cardsArray = cardDetails[i]

        const card = {
            name: cardsArray.mapValue.fields.name.stringValue,
            deviceId: cardsArray.mapValue.fields.deviceId.stringValue
        }
        updatedCards.push(card)
    }

    if (updatedCards.length > 0) {
        return res.send(updatedCards)
    } else {
        return res.status(400).send('No card is associated with the giver user email')
    }
})


// Exporting the router module
module.exports = router