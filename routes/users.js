// Importing express module
const express = require('express')
const router = express.Router()

// Importing database details
const database = require('../startup/database')
const db = database.firestoreDB

// Importing lodash module
const _ = require('lodash')

// Importing bcrypt module
const bcrypt = require('bcrypt')

// Importing jwt generator middleware
const jwtGenerator = require('../middleware/jwtGenerator')

// Importing the authorization middleware
const authorization = require('../middleware/authorization')


// Referencing Users collections
const usersCollectionName = 'Users'

// Referencing Cards collections
const cardsCollectionName = 'Cards'

/* Route for creating a new user in the database */
router.post('/signup', async (req, res) => {
    let userPresent
    const userDetails = _.pick(req.body, ['name', 'email', 'password'])
    const cardsDetails = {
        'name': req.body.name,
        'email': req.body.email,
        'cards': []
    }
    const salt = await bcrypt.genSalt(10)
    userDetails.password = await bcrypt.hash(userDetails.password, salt)

    await db.collection(usersCollectionName).doc(`${req.body.email}`).get()
        .then(user => {
            if (!user.exists) {
                userPresent = false
            } else {
                userPresent = true
            }
        })

    if (req.body.name == null || req.body.name == "" || req.body.email == null || req.body.email == "" || req.body.password == null || req.body.password == "") {
        return res.status(400).send(`Please fill up all the fields`)
    } else if (userPresent) {
        return res.status(400).send(`User with the given email is already present`)
    } else {
        await db.collection(usersCollectionName).doc(`${req.body.email}`).set(userDetails)
        db.collection(cardsCollectionName).doc(`${req.body.email}`).set(cardsDetails)
            .then(() => {
                const token = jwtGenerator.generateJwt(userDetails.email)
                return res.send({
                    name: `${req.body.name}`,
                    email: `${req.body.email}`,
                    token: `${token}`
                })
            })
    }
})

/* Route for getting all the details of a particular user */
router.get('/me', authorization, async (req, res) => {
    let userPresent
    let userDetails

    await db.collection(usersCollectionName).doc(`${req.user.email}`).get()
        .then(user => {
            if (!user.exists) {
                userPresent = false
            } else {
                userPresent = true
                userDetails = user
            }
        })

    if (userPresent) {
        res.status(200).send(_.pick(userDetails._fieldsProto, ['name', 'email']))
    } else {
        res.status(400).send(`User with the given email is not present.`)
    }
})

/* Route for deleting a particular user from the database */
router.delete('/me/delete', authorization, async (req, res) => {
    let userPresent
    let userDetails

    await db.collection(usersCollectionName).doc(`${req.user.email}`).get()
        .then(user => {
            if (!user.exists) {
                userPresent = false
            } else {
                userPresent = true
                userDetails = user
            }
        })

    if (userPresent) {
        const userStatus = await db.collection(usersCollectionName).doc(`${req.user.email}`).delete()
        const cardsStatus = await db.collection(cardsCollectionName).doc(`${req.user.email}`).delete()

        if (userStatus && cardsStatus) {
            res.status(200).send(_.pick(userDetails._fieldsProto, ['name', 'email']))
        } else {
            res.status(500).send(`Something failed.`)
        }
    } else {
        res.status(400).send(`User with the given email is not present.`)
    }
})


// Exporting the router module
module.exports = router