// Importing express module
const express = require('express')
const router = express.Router()

// Importing database details
const database = require('../startup/database')
const db = database.realTimeDB

// Importing the authorization middleware
const authorization = require('../middleware/authorization')


// Referencing Devices collections
const ref = db.ref("/Devices")

/* Route for creating a new device in the database with a unique device id */
router.get('/signup', async (req, res) => {
    let deviceRef = ref.child(`device${req.body.macId}`)

    const deviceDetails = {
        macId: req.body.macId,
        configurations: {
            gasVolume: "29",
            firstNotifiableVolume: "15",
            notificationFrequencyVolume: "5"
        },
        gasUsageWastageVolume: "0",
        objectIsPresent: false,
        valveIsOpen: false,
        isTaken: false
    }

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            return res.status(400).send(`Device with the given Id is already present.`)
        } else {
            deviceRef.set(deviceDetails).then(() => {
                return res.send(deviceDetails)
            })
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for the device to make a request after every 1litre of gas usage or wastage */
router.get('/:deviceId/usage', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            const configuredVolume = (snapshot.val()).configurations.gasVolume
            let usageWastageVolume = (snapshot.val()).gasUsageWastageVolume
            let updatedVolume = parseInt(usageWastageVolume)

            if (parseInt(usageWastageVolume) < parseInt(configuredVolume)) {
                updatedVolume++
            } else if (parseInt(usageWastageVolume) >= parseInt(configuredVolume)) {
                updatedVolume = updatedVolume
            }

            deviceRef.update({
                "gasUsageWastageVolume": `${updatedVolume}`
            }).then(() => {
                res.send({
                    "gasUsageWastageVolume": `${updatedVolume}`
                })
            })
        } else {
            res.status(400).send(`Device with the given Id is not present.`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for getting all the details of a particular device */
router.get('/:deviceId', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            return res.send(snapshot.val())
        } else {
            return res.status(400).send(`Device with the given Id is not present`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for the device to make request when the IR sensor is active or inactive */
router.get('/:deviceId/objectStatus/:status', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)
    let objectStatus

    if (req.params.status == 0) {
        objectStatus = true
    } else if (req.params.status == 1) {
        objectStatus = false
    }

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            deviceRef.update({
                "objectIsPresent": objectStatus
            }).then(() => {
                res.send({
                    "objectIsPresent": objectStatus
                })
            })
        } else {
            res.status(400).send(`Device with the given Id is not present.`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for the device to make a request when the valve is active or inactive */
router.get('/:deviceId/valveStatus/:status', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)
    let valveStatus

    if (req.params.status == 1) {
        valveStatus = true
    } else if (req.params.status == 0) {
        valveStatus = false
    }

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            deviceRef.update({
                "valveIsOpen": valveStatus
            }).then(() => {
                res.send({
                    "valveIsOpen": valveStatus
                })
            })
        } else {
            res.status(400).send(`Device with the given Id is not present.`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for the device to make a request to reset the usageWastageVolume when the reset button is pressed */
router.get('/:deviceId/reset', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            deviceRef.update({
                "gasUsageWastageVolume": "0"
            }).then(() => {
                res.send({
                    "gasUsageWastageVolume": "0"
                })
            })
        } else {
            res.status(400).send(`Device with the given Id is not present.`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for updating the configuration details of a particular device  */
router.get('/:deviceId/configure', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            deviceRef.update({
                "configurations": {
                    "gasVolume": `${req.body.gasVolume}`,
                    "firstNotifiableVolume": `${req.body.firstNotifiableVolume}`,
                    "notificationFrequencyVolume": `${req.body.notificationFrequencyVolume}`
                }
            }).then(() => {
                return res.send({
                    "configurations": {
                        "gasVolume": `${req.body.gasVolume}`,
                        "firstNotifiableVolume": `${req.body.firstNotifiableVolume}`,
                        "notificationFrequencyVolume": `${req.body.notificationFrequencyVolume}`
                    }
                })
            })
        } else {
            return res.status(400).send(`Device with the given Id is not present`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})

/* Route for deleting a particular device from the database */
router.delete('/:deviceId', async (req, res) => {
    let deviceRef = ref.child(`device${req.params.deviceId}`)

    deviceRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            deviceRef.set(null).then(() => {
                res.send('Device with the given Id has been deleted')
            })
        } else {
            res.status(400).send(`Device with the given Id is not present`)
        }
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
})


// Exporting the router module
module.exports = router