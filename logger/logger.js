// Importing winston module
const winston = require('winston')

// Defining the logging format for winston
const {
    format
} = winston

// Configuring the logging format for winston
const {
    combine,
    label,
    json,
    timestamp
} = format


// Configuring a custom error logger
winston.loggers.add('logger', {
    format: combine(label({
        label: 'logger'
    }), timestamp(), json()),
    transports: [
        new winston.transports.Console({
            level: 'silly',
            colorize: true,
            prettyPrint: true
        }),
        new winston.transports.File({
            filename: './logs/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: './logs/info.log',
            level: 'info'
        }),
        new winston.transports.File({
            filename: './logs/verbose.log',
            level: 'verbose'
        }),
        new winston.transports.File({
            filename: './logs/debug.log',
            level: 'debug'
        })
    ]
})

// Creating the logger object from the custom winston error logger
const logger = winston.loggers.get('logger')


// Exporting the logger object
module.exports = logger