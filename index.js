// Importing the error logger module
require('./startup/uncaughtErrorLogger')()

// Importing the custom winston error logger
const logger = require('./logger/logger')

// Importing express module
const express = require('express')
const app = express()

// Importing config module
require('./startup/config')

// Importing the database module
require('./startup/database')

// Importing routes module
require('./startup/routes')(app)

// Importing the production module
require('./startup/production')(app)


// Initializing all the middlewares
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})


// Defining all the routes
app.get('/api/ping', (req, res) => {
    console.log(`Server ping requested...`)
    res.status(200).send({
        success: true,
        data: {
            "project_name": "Browl",
            "project_description": "B.Tech Major Project Server",
            "project_group": "group 39",
            "batch_name": "Batch 2016-2020",
            "department_name": "Electronics & Communications Engineering",
            "college_name": "University of Engineering & Management, Kolkata"
        }
    })
})


// Defining a port for the server to use
const port = process.env.PORT || 3000

// Initializing the server
app.listen(port, () => logger.log('info', `Server started on port ${port}....`))