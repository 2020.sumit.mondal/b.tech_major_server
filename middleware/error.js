// Importing the custom winston error logger
const logger = require('../logger/logger')


// Exporting the error handler middleware
module.exports = function (err, req, res, next) {
    logger.log('error', err.message)
    res.status(500).send(`Internal server error...Something has failed.`)
}