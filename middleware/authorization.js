// Importing json web token module
const jwt = require('jsonwebtoken')

// Importing the config module
const config = require('config')


// Function for validating the jwt
module.exports = function (req, res, next) {
    const token = req.header('x-auth-token')

    if (!token) {
        return res.status(401).send(`Access denied. No token provided.`)
    } else {
        try {
            const decoded = jwt.verify(token, config.get('jwtPrivateKey'))
            req.user = decoded
            next()
        } catch {
            res.status(400).send(`Invalid token.`)
        }
    }
}