// Importing config module
const config = require('config')

// Importing json web token module
const jwt = require('jsonwebtoken')


// Function for generating the json web token
function generateJwt(email) {
    return jwt.sign({
        email: email
    }, config.get('jwtPrivateKey'))
}


// Exporting the generateJwt function
exports.generateJwt = generateJwt